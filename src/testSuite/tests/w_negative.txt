;*************************************************************
;*************************************************************
;**                                                         **
;**                        Lambert W                        **
;**                                                         **
;*************************************************************
;*************************************************************
In: FL_SPCRES=0 FL_CPXRES=0 SD=0 RM=0 IM=2compl SS=4 WS=64
Func: fnWnegative



;=======================================
; W_1(Long Integer) --> Real
;=======================================
In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 FL_SPCRES=1 RX=LonI:"0"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=LonI:"0" RX=Real:"-inf"

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 FL_SPCRES=0 RX=LonI:"0"
Out: EC=5 FL_CPXRES=0 FL_ASLIFT=0 RX=LonI:"0"

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RX=LonI:"1"
Out: EC=1 FL_CPXRES=0 FL_ASLIFT=0 RX=LonI:"1"




;=======================================
; W_1(Time) --> Error24
;=======================================



;=======================================
; W_1(Date) --> Error24
;=======================================



;=======================================
; W_1(String) --> Error24
;=======================================
In:  FL_ASLIFT=0 RX=Stri:"String test"
Out: EC=24 FL_ASLIFT=0 RX=Stri:"String test"



;=======================================
; W_1(Real Matrix) --> Real Matrix
;=======================================
In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RX=ReMa:"M2,3[-0.1,-0.15,-0.2,-0.25,-0.3,-0.35]"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=ReMa:"M2,3[-0.1,-0.15,-0.2,-0.25,-0.3,-0.35]" RX=ReMa:"M2,3[-3.57715206395729721840939196351199488040,-2.99359498677437786687749239304535975649,-2.54264135777352642429380615666184829016,-2.15329236411034964916909915009298137554,-1.78133702342162761197417028151274526082,-1.34971725219224883338314445944635709819]"



;=======================================
; W_1(Complex Matrix) --> Complex Matrix
;=======================================
In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RX=CxMa:"M2,3[-0.1,-0.15,-0.2,-0.25,-0.3,-0.35]"
Out: EC=0 FL_CPXRES=1 FL_ASLIFT=1 RL=CxMa:"M2,3[-0.1,-0.15,-0.2,-0.25,-0.3,-0.35]" RX=CxMa:"M2,3[-3.57715206395729721840939196351199488040,-2.99359498677437786687749239304535975649,-2.54264135777352642429380615666184829016,-2.15329236411034964916909915009298137554,-1.78133702342162761197417028151274526082,-1.34971725219224883338314445944635709819]"



;=======================================
; W_1(Short Integer) --> Error24
;=======================================



;=======================================
; W_1(Real) --> Real
;=======================================
In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RX=Real:"-0.36"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=Real:"-0.36" RX=Real:"-1.222770133978505953142938073423862"

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RX=Real:"-0.1"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=Real:"-0.1" RX=Real:"-3.577152063957297218409391963511995"

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RX=Real:"-0.1":DEG
Out: EC=24 FL_CPXRES=0 FL_ASLIFT=0 RX=Real:"-0.1":DEG



;=======================================
; W_1(Complex) --> Complex
;=======================================
In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RX=Cplx:"-0.36 i 0"
Out: EC=0 FL_CPXRES=1 FL_ASLIFT=1 RL=Cplx:"-0.36 i 0" RX=Cplx:"-1.222770133978505953142938073423862 i 0"

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RX=Cplx:"-0.1 i 0"
Out: EC=0 FL_CPXRES=1 FL_ASLIFT=1 RL=Cplx:"-0.1 i 0" RX=Cplx:"-3.577152063957297218409391963511995 i 0"

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RX=Cplx:"0 i 1"
Out: EC=1 FL_CPXRES=0 FL_ASLIFT=0 RX=Cplx:"0 i 1"
